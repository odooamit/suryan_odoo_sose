
import math

from openerp.osv import osv, fields

import openerp.addons.product.product


class res_users(osv.osv):
    _inherit = 'res.partner'

    def _pos_order_count(self, cr, uid, ids, field_name, arg, context=None):
        res = dict(map(lambda x: (x,0), ids))
        # The current user may not have access rights for sale orders
        try:
            for partner in self.browse(cr, uid, ids, context):
                res[partner.id] = len(partner.sale_order_ids) + len(partner.mapped('pos_order_ids'))
        except:
            pass
        return res

    _columns = {
        'ean13' : fields.char('EAN13', size=13, help="BarCode"),
        'pos_order_count': fields.function(_pos_order_count, string='# of POS Order', type='integer'),
        'pos_order_ids': fields.one2many('pos.order','partner_id','POS Order')
    }

    def _check_ean(self, cr, uid, ids, context=None):
        return all(
            openerp.addons.product.product.check_ean(user.ean13) == True
            for user in self.browse(cr, uid, ids, context=context)
        )

    _constraints = [
        (_check_ean, "Error: Invalid ean code", ['ean13'],),
    ]

