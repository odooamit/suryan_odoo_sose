# -*- coding: utf-8 -*-

{
    'name': 'Website product Sorting',
    'version': '1.0',
    'category': 'Ecommerce',
    'description': """
This module add option to sort product in website
""",
    'author': 'DRC Systems India Pvt. Ltd.',
    'images': ['/website_product_sort/static/description/icon.png'],
    'website': 'https://http://www.drcsystems.com/',
    'depends': ['website', 'website_sale', 'website_product_brands'],
    'data': [
        'views/website_templates.xml',
    ],
    'installable': True,
    'auto_install': False,
}
