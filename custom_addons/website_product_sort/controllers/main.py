import collections

from openerp import http
from openerp.http import request
from openerp.addons.website_sale.controllers import main as website_sale


class WebsiteSale(website_sale.website_sale):

    @http.route()
    def shop(self, page=0, category=None, search='', **kw):
        response = super(WebsiteSale, self).shop(page=page, category=category, search=search, **kw)

        domain = request.website.sale_product_domain()
        if search:
            for srch in search.split(" "):
                domain += ['|', '|', '|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch), ('website_description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch), ('product_brand_id.name', 'ilike', srch)]
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]

        pager = response.qcontext['pager']

        # Add field which you want to sort by
        sort_option = {1: ['name', 'name', 'Name[A-Z]'], 2: ['name desc', 'name_inv', 'Name[Z-A]'], 3: ['list_price', 'price', 'Price[Low-High]'], 4: ['list_price desc', 'price_inv', 'Price[High-Low]']}

        sorting = int(kw.get('sorting', 0))
        if sorting:
            order = 'website_published desc, %s, website_sequence desc' % sort_option.get(sorting)[0]
        else:
            order = 'website_published desc, name, website_sequence desc'

        brand = int(kw.get('brand', 0))
        if brand:
            domain += [('product_brand_id', '=', brand)]
        products = request.env['product.template'].search(domain, limit=website_sale.PPG, offset=pager['offset'], order=order)

        response.qcontext['products'] = products
        response.qcontext['bins'] = website_sale.table_compute().process(products)
        response.qcontext['sorting'] = sorting

        sorting_type = collections.OrderedDict(sorted(sort_option.items()))
        response.qcontext['sort_option'] = sorting_type
        response.qcontext['brand'] = brand
        return response
