# -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#	See LICENSE file for full copyright and licensing details.
#################################################################################

from openerp import api, fields , models
import logging
from openerp.exceptions import Warning
from openerp.addons.website.models.website import slugify, slug
from openerp.osv import orm
from openerp.tools import html_escape as escape, ustr, image_resize_and_sharpen, image_save_for_web
_logger = logging.getLogger(__name__)

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from PIL import Image
from openerp.exceptions import Warning
from openerp import tools

class product_template(models.Model):
	_inherit = 'product.template'
	product_brand_id = fields.Many2one(string='Brand', comodel_name="wk.product.brand")	
class product_public_category(models.Model):
	_inherit = "product.public.category"

class Wk_ProductBrand(models.Model):	
	@api.one
	@api.depends('products')
	def _get_product_count(self):		
		self.total_products = len(self.products.ids)
	_name = "wk.product.brand"
	name = fields.Char(string="Brand Name",required=True, translate=True)
	image = fields.Binary('product  image', help = " This Image will be visible to user in there shop  by brand on website view !")
	description = fields.Text(string ="Brand Description", translate=True)
	sequence = fields.Integer(string = "Sequence" ,help = "Gives the sequence order when displaying a list of brand on website view.!")
	products = fields.One2many('product.template','product_brand_id')
	total_products = fields.Integer(string="Total no. of Products",compute=_get_product_count)

	@api.onchange('image')
	def _onchange_image(self):
		if self.image:
			image_minimum_width = 200
			image_minimum_height = 100
			base64_source = tools.image_resize_image_big(self.image)
			image_stream = StringIO.StringIO(base64_source.decode('base64'))
			image = Image.open(image_stream)
			size = image.size
			ratio = round(size[0] / float(size[1]),2)
			if 1.9 > ratio or ratio >= 2.1:
				raise Warning('Maintain approximate ratio:- 2:1')
			if size[0] < image_minimum_width or size[1] < image_minimum_height:
				raise Warning('Minimum Image Size: %s*%s' % (image_minimum_width, image_minimum_height))

	# @api.multi
	# def write(self, vals):
	# 	if vals.get('image'):
			# image_minimum_width = 200
			# image_minimum_height = 100
			# base64_source = tools.image_resize_image_big(vals.get('image'))
			# image_stream = StringIO.StringIO(base64_source.decode('base64'))
			# image = Image.open(image_stream)
			# size = image.size
			# ratio = round(size[0] / float(size[1]),2)
			# if 1.9 > ratio or ratio >= 2.1:
			# 	raise Warning('Maintain approximate ratio:- 2:1')
			# if size[0] < image_minimum_width or size[1] < image_minimum_height:
			# 	raise Warning('Minimum Image Size: %s*%s' % (image_minimum_width, image_minimum_height))
	# 	return super(Wk_ProductBrand, self).write(vals)

	
	
	