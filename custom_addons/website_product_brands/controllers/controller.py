# -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#################################################################################
# import werkzeug
from openerp import http
from openerp.http import request
from openerp.addons.website.controllers.main import Website as Home
from openerp.addons.website_sale.controllers import main as website_sale
# from openerp import SUPERUSER_ID
# from openerp.addons.website.models.website import slug
# from openerp.addons.website_sale.controllers.main import table_compute, QueryURL
# import logging
# _logger = logging.getLogger(__name__)
# from openerp.exceptions import Warning
# PPG = 20
# PPR = 4
# PPG = 20
# PPR = 4

class Website(Home):

    @http.route()
    def index(self, *args, **kw):
        cr, uid, context = request.cr, request.uid, request.context
        response = super(Website, self).index(*args, **kw)

        brand = request.registry['wk.product.brand']
        brand_ids = brand.search(cr, uid, [], context=context, limit=4)
        brand_obj = brand.browse(cr, uid, brand_ids, context=context)
        response.qcontext['col1_brand'] = brand_obj

        brand_ids1 = brand.search(cr, uid, [('id', 'not in', brand_ids)], context=context, limit=6)
        brand_obj1 = brand.browse(cr, uid, brand_ids1, context=context)
        response.qcontext['col2_brand'] = brand_obj1

        return response


class WebsiteSale(website_sale.website_sale):

    @http.route()
    def shop(self, page=0, category=None, search='', **kw):
        cr, uid, context = request.cr, request.uid, request.context
        response = super(WebsiteSale, self).shop(page=page, category=category, search=search, **kw)

        brand = request.registry['wk.product.brand']
        brand_ids = brand.search(cr, uid, [], context=context)
        brand_obj = brand.browse(cr, uid, brand_ids, context=context)
        response.qcontext['brand_list'] = brand_obj
        return response

#     @http.route(
#         ['/shop',
#          '/shop/page/<int:page>',
#          '/shop/category/<model("product.public.category"):category>',
#          '/shop/category/<model("product.public.category"):category>/'
#          'page/<int:page>'],
#         type='http', auth="public", website=True)
#     def shop(self, page=0, category=None, search='',  **post):
#         brand = post.has_key('brand')  and post.get('brand') or False       
#         cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
#         values = {}
#         parent_category_ids = []
#         domain = request.website.sale_product_domain() 
#         if search:
#             domain += ['|', '|', '|','|',
#                        ('name', 'ilike', search),
#                        ('description', 'ilike', search),
#                        ('description_sale', 'ilike', search),
#                        ('product_variant_ids.default_code', 'ilike', search),
#                        ('product_brand_id.name', 'ilike', search)]       
       
#         if category:
#             parent_category_ids = [category.id]
#             current_category = category
#             while current_category.parent_id:
#                 parent_category_ids.append(current_category.parent_id.id)
#                 current_category = current_category.parent_id
#             domain += [('public_categ_ids', 'child_of', int(category))]
#         attrib_list = request.httprequest.args.getlist('attrib')
#         attrib_values = [map(int, v.split('-')) for v in attrib_list if v]        
#         attrib_set = set([v[1] for v in attrib_values])        
#         if attrib_values:
#             attrib = None
#             ids = []
#             for value in attrib_values:
#                 if not attrib:
#                     attrib = value[0]
#                     ids.append(value[1])
#                 elif value[0] == attrib:
#                     ids.append(value[1])
#                 else:
#                     domain += [('attribute_line_ids.value_ids', 'in', ids)]
#                     attrib = value[0]
#                     ids = [value[1]]
#             if attrib:
#                 domain += [('attribute_line_ids.value_ids', 'in', ids)]
#         keep = QueryURL('/shop', category=category and int(category), search=search, attrib=attrib_list)
#         #######################################################################################     
#         brand_list = request.httprequest.args.getlist('attrib_brand')        
#         brand_values = [map(int, b.split('-')) for b in brand_list if b] 
#         domain , brand_set = self._get_brand_domain(domain,brand_values)       
#         ################################################################################################
#         if not context.get('pricelist'):
#             pricelist = self.get_pricelist()
#             context['pricelist'] = int(pricelist)
#         else:
#             pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)
#         product_obj = pool.get('product.template')
#         url = '/shop'
#         product_count = product_obj.search_count(cr, uid, domain, context=context)
#         if search:
#             post['search'] = search
#         if category:
#             category = pool['product.public.category'].browse(cr, uid, int(category), context=context)
#             url = '/shop/category/%s' % slug(category)
#         pager = request.website.pager(url=url, total=product_count, page=page, step=PPG, scope=7, url_args=post)
#         product_ids = product_obj.search(cr, uid, domain, limit=PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
#         products = product_obj.browse(cr, uid, product_ids, context=context)
        
#         brands = self._get_brand(products)  
#         style_obj = pool['product.style']
#         style_ids = style_obj.search(cr, uid, [], context=context)
#         styles = style_obj.browse(cr, uid, style_ids, context=context)
#         category_obj = pool['product.public.category']
#         category_ids = category_obj.search(cr, uid, [], context=context)
#         categories = category_obj.browse(cr, uid, category_ids, context=context)
#         categs = filter(lambda x: not x.parent_id, categories)
#         if category:
#             selected_id = int(category)
#             child_prod_ids = category_obj.search(cr, uid, [('parent_id', '=', selected_id)], context=context)
#             children_ids = category_obj.browse(cr, uid, child_prod_ids)
#             values.update({'child_list': children_ids})
#         attributes_obj = request.registry['product.attribute']
#         attributes_ids = attributes_obj.search(cr, uid, [], context=context)
#         attributes = attributes_obj.browse(cr, uid, attributes_ids, context=context)
#         from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
#         to_currency = pricelist.currency_id
#         compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)    
#         keep = QueryURL('/shop')   
#         values.update({'search': search,
#                        'category': category,
#                        'brand':brand,
#                        'brand_rec':request.env['wk.product.brand'].search([]),
#                        'parent_category_ids': parent_category_ids,
#                        'brands':brands,
#                        'attrib_values': attrib_values,
#                        'attrib_set': attrib_set,
#                        'brand_set':brand_set,
#                        'pager': pager,
#                        'pricelist': pricelist,
#                        'products': products,
#                        'bins': table_compute().process(products),
#                        'rows': PPR,
#                        'styles': styles,                       
#                        'categories': categs,
#                        'categ.id':categs,
#                        'attributes': attributes,
#                        'compute_currency': compute_currency,
#                        'keep': keep,
#                        'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
#                        'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib', i) for i in attribs])})
#         return request.website.render('website_sale.products', values)     


#     def _get_brand_domain(self,domain, brand_values):
#         brand_set = set([b[1] for b in brand_values])
#         if brand_values: 
#             domain += [('product_brand_id.id', 'in', list(brand_set))]
#         return domain , brand_set        
#     def _get_brand(self, products):
#         product_brands = []
#         [product_brands.append(product.product_brand_id) for product  in products if len(product.product_brand_id) and product.product_brand_id not in product_brands]  
#         return product_brands
# class wk_website_sale(website_sale):
#     def _get_brand_domain(self,domain,brand_values):         
#         brand =request.httprequest.args.getlist('brand')
#         brands =request.httprequest.args.getlist('brands')       
#         if brands and brand:
#             brand_list = map(int, brand)
#             brands_list = map(int, brands[0][:-1].split(','))
#             res =[]
#             for temp in brands_list:
#                 if temp not in brand_list:
#                     res.append(temp)
#             domain += [('product_brand_id.id', 'in', res)]
#             return domain, set(res)
#         else:          
#             return super(wk_website_sale, self)._get_brand_domain(domain,brand_values)



       

