# -*- coding: utf-8 -*-

from openerp import models, fields, api

class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def _compute_pos_order(self):
        for partner in self:
            partner.pos_order_count = len(partner.pos_order_ids) + len(partner.mapped('child_ids.pos_order_ids'))

    pos_order_count = fields.Integer(compute='_compute_pos_order', string='# of POS Order')
    pos_order_ids = fields.One2many('pos.order','partner_id','POS Order')

