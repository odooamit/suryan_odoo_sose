# -*- coding: utf-8 -*-

{
    'name': 'POS Order Count',
    'version': '1.0',
    'description': """
This module added stat button that display pos order in customer view
""",
    'data': [
        'views/res_partner_view.xml'
    ],
    'author': 'DRC Systems',
    'depends': ['point_of_sale'],
    'installable': True,
}
