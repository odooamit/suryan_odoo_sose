# -*- coding: utf-8 -*-
import re
import urlparse

from openerp import models, fields, api
from openerp.osv import orm


DEFAULT_CDN_FILTERS = [
    "^/[^/]+/static/",
    "^/web/(css|js)/",
    "^/web/image",
    "^/web/content",
    # retrocompatibility
    "^/website/image/",
]


class CDN(models.Model):
    _inherit = 'website'

    cdn_activated = fields.Boolean(string='Use a Content Delivery Network (CDN)')
    cdn_url = fields.Char(string='CDN Base URL')
    cdn_filters = fields.Text(string='CDN Filters')

    def get_cdn_url(self, cr, uid, uri, request, context=None):
        # Currently only usable in a website_enable request context
        if request and request.website and not request.debug and request.website.user_id.id == request.uid:
            cdn_url = request.website.cdn_url
            cdn_filters = (request.website.cdn_filters or '').splitlines()
            for flt in cdn_filters:
                if flt and re.match(flt, uri):
                    return urlparse.urljoin(cdn_url, uri)
        return uri

class CDNSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    _name = 'cdn.config.settings'

    cdn_activated = fields.Boolean(string='Use CDN')
    cdn_url = fields.Char(string='CDN Base URL')
    cdn_filters = fields.Text(string='CDN Filters')

    @api.model
    def get_default_cdn_configuration(self, fields):
        cdn = self.env['website'].search([('id', '=', 1)], limit=1)
        """if not cdn:
            cdn = self.env['website'].create({
                'cdn_activated': False,
                'cdn_url': '//localhost:8069/',
                'cdn_filters': '\n'.join(DEFAULT_CDN_FILTERS),
            })"""

        return {
            'cdn_activated': cdn.cdn_activated,
            'cdn_url': cdn.cdn_url,
            'cdn_filters': cdn.cdn_filters
        }

    @api.one
    def set_cdn_configuration(self):
        cdn = self.env['website'].search([('id', '=', 1)], limit=1)
        cdn.write({
            'cdn_activated': self.cdn_activated,
            'cdn_url': self.cdn_url,
            'cdn_filters': self.cdn_filters
        })

