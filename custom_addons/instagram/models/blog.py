# -*- coding: utf-8 -*-

from openerp import models, fields


class Blog(models.Model):
    _inherit = 'blog.post'

    description = fields.Char()
   