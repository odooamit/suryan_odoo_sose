# -*- coding: utf-8 -*-

from openerp import models, api


class Website(models.Model):
    _inherit = 'website'

    @api.model
    def get_categ(self):
        category = self.env['product.public.category'].search([])
        return category
