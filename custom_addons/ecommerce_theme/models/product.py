# -*- coding: utf-8 -*-
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from PIL import Image
from openerp.exceptions import Warning
from openerp import tools
from openerp import models, fields, api


class Product(models.Model):
    _inherit = 'product.template'

    is_featured = fields.Boolean("Featured")
    is_new = fields.Boolean()
    product_weight = fields.Char()

    @api.onchange('image_medium')
    def _onchange_image(self):
        if self.image_medium:
            image_minimum_width = 630
            image_minimum_height = 400
            base64_source = tools.image_resize_image_big(self.image_medium)
            image_stream = StringIO.StringIO(base64_source.decode('base64'))
            image = Image.open(image_stream)
            size = image.size
            ratio = round(size[0] / float(size[1]),2)
            if 1.5 > ratio or ratio >= 1.595:
                raise Warning('Maintain approximate ratio for other Product images:- 1.5:1')
            if size[0] < image_minimum_width or size[1] < image_minimum_height:
                raise Warning('Minimum Image Size for other Product images: %s*%s' % (image_minimum_width, image_minimum_height))


    # @api.multi
    # def write(self, vals):
    #     if vals.get('image_medium'):
    #         image_minimum_width = 630
    #         image_minimum_height = 400
    #         base64_source = tools.image_resize_image_big(vals.get('image_medium'))
    #         image_stream = StringIO.StringIO(base64_source.decode('base64'))
    #         image = Image.open(image_stream)
    #         size = image.size
    #         ratio = round(size[0] / float(size[1]),2)
    #         if 1.5 > ratio or ratio >= 1.595:
    #             raise Warning('Maintain approximate ratio:- 1.5:1')
    #         if size[0] < image_minimum_width or size[1] < image_minimum_height:
    #             raise Warning('Minimum Image Size: %s*%s' % (image_minimum_width, image_minimum_height))
    #     return super(Product, self).write(vals)

class ProductCategory(models.Model):
    _inherit = 'product.public.category'

    category_description = fields.Text()

class PartnerCity(models.Model):
    _name = "res.partner.city"
    _description = "partner's city"

    name = fields.Char()

    _sql_constraints = [
        ('city_unique', 'unique (name)', 'City must be unique')
    ]

class ResPartner(models.Model):
    _inherit = 'res.partner'

    city_id = fields.Many2one('res.partner.city', 'City', ondelete="cascade")

    @api.onchange('city_id')
    def _onchange_city_id(self):
        self.city = self.city_id.name