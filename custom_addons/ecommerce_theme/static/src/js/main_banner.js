
$(".small-btn").click(function(){
    $(".nav-link").slideToggle();  
});
$(".categorie-box .category_arrow").click(function(){
	if($(this).parent("h2").next("ul").is(":visible"))
	{
		$(this).children(".fa").removeClass("fa-angle-up");
		$(this).children(".fa").addClass("fa-angle-down");
		$(this).parent("h2").next("ul").slideUp();
	}
	else
	{
		$(this).children(".fa").removeClass("fa-angle-down");
		$(this).children(".fa").addClass("fa-angle-up");
		$(this).parent("h2").next("ul").slideDown();
	}
});
 var mainBanner = $("#mainBanner");
    mainBanner.owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    items:1,
    autoplay: true,
});
$('.integrations-slider').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots:true,
    items:1,
});

function setupLabel() {
    if ($('.label_check input').length) {
        $('.label_check').each(function(){ 
            $(this).removeClass('c_on');
        });
        $('.label_check input:checked').each(function(){ 
            $(this).parent('.label_check').addClass('c_on');
        });                
    };
    if ($('.label_radio input').length) {
        $('.label_radio').each(function(){ 
            $(this).removeClass('r_on');
        });
        $('.label_radio input:checked').each(function(){ 
            $(this).parent('label').addClass('r_on');
        });
    };
};

if ($(document.body).find('#oe_main_menu_navbar').length == 1) {
    $('#nav-main').css("top", "34px");
}
// Taken from old
$('div.categorie-box a[data-toggle="collapse"]').click(function(){
    $(this).find('i').toggleClass("fa-angle-right fa-angle-down");
});


$('.gallery-box').click( function() { 
    window.location.href= $(this).find('a').attr('href');
});

// Taken from old

$(window).scroll(function(){
    if($(".gallery-view").offset()) {
        var gallery_top = $(".gallery-view").offset().top;  
        var window_height = $(window).height();
        gallery_top = gallery_top - (window_height / 2)   ;
        if ($(window).scrollTop() > gallery_top)
        {
            $(".gallery-view .gallery-box").addClass("show");
        }
        else
        {
            $(".gallery-view .gallery-box").removeClass("show");
        }
    }
    if($(".news-view").offset()){
        var news_top = $(".news-view").offset().top;
        news_top = news_top - (window_height / 2); 
        if ($(window).scrollTop() > news_top)
        {
            $(".news-view .news-box").addClass("show");
        }
        else
        {
            $(".news-view .news-box").removeClass("show");
        }
    }
    if($(".our-functionality").offset()){
        var functionality_top = $(".our-functionality").offset().top;
        functionality_top = functionality_top - (window_height / 1.2); 
        if ($(window).scrollTop() > functionality_top)
        {
            $(".our-functionality .left-text").addClass("show");
            $(".our-functionality .right-text").addClass("show");
        }
        else
        {
            $(".our-functionality .left-text").removeClass("show");
            $(".our-functionality .right-text").removeClass("show");
        }
    }
    
});
$(document).ready(function() {
    $( ".old_city:first" ).val($( ".added_city_id:first option:selected:first" ).text());
    $( ".added_city_id" ).change(function() {
        $(this).siblings( ".old_city" ).val( $(this).find("option:selected").text());
    });

    $("#myCarousel").css ("width",$(window).width());
    $('.label_check, .label_radio').click(function(){
        setupLabel();
    });
    setupLabel();
    $('.oe_website_sale').each(function () {
        var oe_website_sale = this;

        var $shippingDifferent = $("input[name='shipping_id']", oe_website_sale);
        $shippingDifferent.change(function (event) {
            var value = +$(this).val();
            var data = $(this).data();
            var $snipping = $(".js_shipping", oe_website_sale);
            var $inputs = $snipping.find("input");
            var $selects = $snipping.find("select");

            $snipping.toggle(!!value);
            $inputs.attr("readonly", value <= 0 ? null : "readonly" ).prop("readonly", value <= 0 ? null : "readonly" );
            $selects.attr("disabled", value <= 0 ? null : "disabled" ).prop("disabled", value <= 0 ? null : "disabled" );

            $inputs.each(function () {
                $(this).val( data[$(this).attr("name")] || "" );
            });
        });
    });

    $(".categorie-box").find('li .active').parent().addClass('in')
    $(".categorie-box").find('li .active').parent().css('height', 'auto')
    $(".categorie-box").find('li .active').parent().siblings('a').removeClass('collapsed')

if($(window).width()>991)
{
    var window_height = $(window).height();
    $("#myCarousel .carousel-inner .item").css ("height",window_height + "px");
}
else
{
	var slider_width = ($(window).width() * 43.89) / 100 ;
	$("#myCarousel .carousel-inner .item").css ("height",slider_width + "px"); 
}
     $("#myCarousel .carousel-inner .item").each(function () {
        var img_src = $(this).children("img").attr('src');
        $(this).css("background-image", 'url(' + img_src + ')');
        $(this).children("img").hide();
    });

    // var window_height = $(window).height()-50;
    // $("#myCarousel").css ("height",window_height + "px");
    //  $("#myCarousel .carousel-inner").each(function () {
    //     var img_src = $(this).children(".item").children("img").attr('src');
    //     $(this).children(".item").css("background-image", 'url(' + img_src + ')');
    //     $(this).children(".item").children("img").hide();
    // });

    $(".bottom-nav").css("top", $(window).height() - 48  + "px");
    
    
    $(".search-block a").click(function(){
        $(".search-block .search-box").slideToggle();
    });
    $(".search-box .icon").click(function(){
        $(".search-block .search-box").slideToggle();
    });
    $("#footer .bottom-arrow").click(function(){
        $("html, body").animate({ scrollTop: 0 },1000); 
    });
    /* manu fiexd js start*/
    if($(window).scrollTop() > 38)  
    {
        $("#nav-main").addClass("fiexd");
        $("#nav-main").css("position","fixed");
    }
    else
    {
        $("#nav-main").removeClass("fiexd");
    }
    if($(".our-functionality").offset()){
        var top_value = $(".our-functionality").offset().top ; 
        top_value = top_value - 48 - $("#nav-main").height(); 
        if ($(window).scrollTop() > top_value)
        {
            $(".bottom-nav").addClass("fiexd");
            $(".bottom-nav").css("top",$("#nav-main").height() + "px");
        }
        else
        {
            $(".bottom-nav").removeClass("fiexd");
            $(".bottom-nav").css("top", $(window).height() - 48  + "px");
        }
    }
    if($(window).width() > 767)
    {
        $("#nav-main").removeClass("fiexd");
    }
    else
    {
        $("#nav-main").addClass("fiexd");
        $("#nav-main").css("position","fixed");

    }
    /* manu fiexd js end*/
    
     $(".slider-nav .left-arrow").click(function() {
        mainBanner.trigger('prev.owl.carousel');
    });
    
    $(".slider-nav .right-arrow").click(function() {
        mainBanner.trigger('next.owl.carousel');
    });
    
});
$(window).resize(function() {
    var window_height = $(window).height();
    $("#myCarousel").css ("width",$(window).width()); 
	
    if ($(".bottom-nav").hasClass("fiexd"))
    {
        
    }
    else
    {
        $(".bottom-nav").css("top", $(window).height() - 48  + "px");
    }
    if($(window).width() > 991)
    {
        $("#nav-main").removeClass("fiexd");
		$("#myCarousel .carousel-inner .item").css ("height",window_height + "px"); 
    }
    else
    {
        $("#nav-main").addClass("fiexd");
		var slider_width = ($(window).width() * 43.89) / 100 ;
		$("#myCarousel .carousel-inner .item").css ("height",slider_width + "px"); 
    }
});
$(window).scroll(function(){
    if($(window).width() > 767)
    {
        if($(window).scrollTop() > 38)  
        {
            $("#nav-main").addClass("fiexd");
            $("#nav-main").css("height","58px");
            if ($(document.body).find('#oe_main_menu_navbar').length == 1) {
                $('#nav-main').css("top", "0px");
            }
        }
        else
        {
            $("#nav-main").removeClass("fiexd");
            $("#nav-main").css("height","96px");
            if ($(document.body).find('#oe_main_menu_navbar').length == 1) {
                $('#nav-main').css("top", "34px");
            }
        }
    }
    else
    {
        $("#nav-main").addClass("fiexd");
        if (window.location.pathname != '/') {
            $("#nav-main").css("height","58px");
        }
        if ($(document.body).find('#oe_main_menu_navbar').length == 1) {
            $('#nav-main').css("top", "0px");
        }
    }
    if($(".our-functionality").offset()){
        var top_value = $(".our-functionality").offset().top ; 
        top_value = top_value - 48 - $("#nav-main").height(); 
        if ($(window).scrollTop() > top_value)
        {
            $(".bottom-nav").addClass("fiexd");
            $(".bottom-nav").css("top",$("#nav-main").height() + "px");
        }
        else
        {
            $(".bottom-nav").removeClass("fiexd");
            $(".bottom-nav").css("top", $(window).height() - 48  + "px");
        }
    }
});
$(document).ready(function() {
	$("#nav-main .nav-link > ul > li > a ").click(function(){
		if($(window).width() > 991)
		{
		}
		else
		{
			if($(this).next(".main-submenu").is(":visible"))
			{
				$(this).next(".main-submenu").slideUp();
				$(this).removeClass("active");
			}
			else
			{
				$("#nav-main .nav-link ul li .main-submenu").slideUp();
				$("#nav-main .nav-link ul li a").removeClass("active");
				$(this).addClass("active");
				$(this).next(".main-submenu").slideDown();
				
			}
		}
	});
});