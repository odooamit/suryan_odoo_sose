# -*- coding: utf-8 -*-

{
    'name': 'Ecommerce Theme',
    'version': '1.0',
    'category': 'Marketting',
    'description': """
This module added sniipet for using in website
""",
    'author': 'DRC Systems',
    'depends': ['website', 'website_sale'],
    'data': [
        'views/website_templates.xml',
        'views/snippets.xml',
        'views/product_views.xml',
        'security/ir.model.access.csv',
    ],
    # 'css': ['static/src/css/*.css'],
    'installable': True,
}
