# -*- coding: utf-8 -*-

{
    'name': 'PayU Money Payment Acquirer',
    'summary': 'Payment Acquirer: PayU Money Implementation',
    'version': '1.0',
    'description': """PayU Money Payment Acquirer""",
    'author': 'DRC Systems',
    'depends': ['payment'],
    'data': [
        'views/payumoney.xml',
        'views/payment_acquirer.xml',
        'data/payment_payumoney_data.xml',
        'data/template_payu.xml',
    ],
    'installable': True,
}
