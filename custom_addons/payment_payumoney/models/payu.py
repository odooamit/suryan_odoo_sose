# -*- coding: utf-'8' "-*-"

import hashlib
import logging
import urlparse

from openerp.addons.payment_payumoney.controllers.main import PayuMoneyController
from openerp.addons.payment.models.payment_acquirer import ValidationError
from openerp.osv import osv, fields
from openerp import SUPERUSER_ID

_logger = logging.getLogger(__name__)


class AcquirerPayumoney(osv.Model):
    _inherit = 'payment.acquirer'

    def _get_payu_urls(self, cr, uid, environment, context=None):
        """ payu URLS """
        if environment == 'prod':
            return {'payu_form_url': 'https://secure.payu.in/_payment'}
        else:
            return {'payu_form_url': 'https://test.payu.in/_payment'}

    def _get_providers(self, cr, uid, context=None):
        providers = super(AcquirerPayumoney, self)._get_providers(cr, uid, context=context)
        providers.append(['payumoney', 'PayuMoney'])
        return providers

    _columns = {
        'payumoney_merchant_key': fields.char(
            'Payumoney Merchant Key',
            help='The Merchant Key is used to ensure communications coming from PayU Money are valid and secured.'),
        'payumoney_merchant_salt': fields.char('Payumoney Merchant Salt')
    }

    def _payumoney_generate_sha(self, acquirer, inout, values):
        if inout == 'in':
            keys = "key|txnid|amount|productinfo|firstname|email|udf1|||||||||".split('|')
            sign = ''.join('%s|' % (values.get(k) or '') for k in keys)
            sign += acquirer.payumoney_merchant_salt or ''
        else:
            keys = "|status||||||||||udf1|email|firstname|productinfo|amount|txnid|key".split('|')
            sign = ''.join('%s|' % (values.get(k) or '') for k in keys)
            sign = acquirer.payumoney_merchant_salt + sign
        return hashlib.sha512(sign).hexdigest()

    def payumoney_form_generate_values(self, cr, uid, id, partner_values, tx_values, context=None):
        base_url = self.pool['ir.config_parameter'].get_param(cr, SUPERUSER_ID, 'web.base.url')
        acquirer = self.browse(cr, uid, id, context=context)

        payumoney_tx_values = dict(tx_values)
        payumoney_tx_values.update({
            'key': acquirer.payumoney_merchant_key,
            'txnid': tx_values['reference'],
            'amount': tx_values['amount'],
            'productinfo': tx_values['reference'],
            'firstname': partner_values['first_name'],
            'email': partner_values['email'],
            'phone': partner_values['phone'],
            'service_provider': 'payu_paisa',
            'surl': '%s' % urlparse.urljoin(base_url, PayuMoneyController._return_url),
            # 'furl': '%s' % urlparse.urljoin(base_url, PayuMoneyController._exception_url),
            # 'curl': '%s' % urlparse.urljoin(base_url, PayuMoneyController._cancel_url)
        })
        payumoney_tx_values['udf1'] = payumoney_tx_values.pop('return_url', '')
        payumoney_tx_values['hash'] = self._payumoney_generate_sha(
            acquirer, 'in', payumoney_tx_values)
        return partner_values, payumoney_tx_values

    def payumoney_get_form_action_url(self, cr, uid, id, context=None):
        acquirer = self.browse(cr, uid, id, context=context)
        return self._get_payu_urls(cr, uid, acquirer.environment, context=context)['payu_form_url']

class TxPayumoney(osv.Model):
    _inherit = 'payment.transaction'

    _columns = {
        'payumoney_response': fields.text('Payumoney Response JSON'),
    }


    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}

        local_context = context.copy()
        template_id = self.pool['ir.model.data'].xmlid_to_res_id(cr, uid, 'payment_payumoney.trans_complete')
        state = vals.get('state', False)
        if state:
            trans = self.browse(cr, uid, ids, context=context)
            if trans.acquirer_id.provider == 'payumoney':
                trans_status = {'pending': 'is in Pending state', 'done': 'is Successful', 'error': 'is not Successful', 'cancel': 'is cancelled'}
                status = trans_status.get(state, 'pending')
                local_context.update({'status': status})
                self.pool['email.template'].send_mail(cr, uid, template_id, trans.id, force_send=True, context=local_context)

        return super(TxPayumoney, self).write(cr, uid, ids, vals, context=context)

    def _payumoney_form_get_tx_from_data(self, cr, uid, data, context=None):
        reference = data.get('txnid')
        tx_ids = self.pool['payment.transaction'].search(cr, uid, [('reference', '=', reference)], context=context)
        if not tx_ids or len(tx_ids) > 1:
            error_msg = 'PayuMoney: received data for reference %s' % (reference)
            if not tx_ids:
                error_msg += '; no order found'
            else:
                error_msg += '; multiple order found'
            _logger.error(error_msg)
            raise ValidationError(error_msg)
        return self.browse(cr, uid, tx_ids[0], context=context)

    def _payumoney_form_validate(self, cr, uid, tx, data, context=None):
        status = data.get('status', 'success')
        if status == 'success':
            self.write(cr, uid, tx.id, {'state': 'done', 'payumoney_response': data}, context=context)
            return True
        elif status == 'pending':
            self.write(cr, uid, tx.id, {'state': 'pending', 'payumoney_response': data}, context=context)
            return True
        else:
            self.write(cr, uid, tx.id, {'state': 'error', 'payumoney_response': data}, context=context)
            return False
