# -*- coding: utf-8 -*-

from openerp import models, fields

class PosConfig(models.Model):
	_inherit = 'pos.config'

	wk_error_msg = fields.Char("Custom Error Message")
	wk_deny_val = fields.Float("Deny Order When Product Stock Is Less Than")
	wk_continous_sale = fields.Boolean("Allow Order When Out-Of-Stock")
