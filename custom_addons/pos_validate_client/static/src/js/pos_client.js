openerp.pos_validate_client = function (instance) {

    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    var module=instance.point_of_sale

    module.PaypadButtonWidget = module.PaypadButtonWidget.extend({
        renderElement: function() {
            var self = this;

            this._super();
            if(self.pos.config.is_customer_mandatory){
                this.$el.unbind('click');

                this.$el.click(function(){
                    if(self.pos.get('selectedOrder').get_client()){
                        if (self.pos.get('selectedOrder').get('screen') === 'receipt'){  //TODO Why ?
                            console.warn('TODO should not get there...?');
                            return;
                        }
                        self.pos.get('selectedOrder').addPaymentline(self.cashregister);
                        self.pos_widget.screen_selector.set_current_screen('payment');
                    }
                    else{
                        alert("Please select Customer First!!!");
                    }
                });
            }
        },
    });
}